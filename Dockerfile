FROM mcr.microsoft.com/dotnet/framework/sdk:4.8 AS build

# Can be overrided during container run
ENV NugetServer_ApiKey="00000000-0000-0000-0000-000000000000"

WORKDIR /app

COPY . .

RUN nuget restore

RUN msbuild /p:Configuration=Release /p:DeployOnBuild=true /p:PublishProfile=FolderProfile.pubxml

# Deploy published app into IIS 

FROM mcr.microsoft.com/dotnet/framework/aspnet:4.8 AS runtime

WORKDIR /inetpub/wwwroot

COPY --from=build /app/nugetserver/bin/release/publish/. ./
# Grant permissions to Package folder on IIS to pull and push packages
RUN ["icacls", "Packages", "/grant", "IIS_IUSRS:(OI)(CI)(RD,WD,F)"]

# Create folder for logs
RUN mkdir logs
# Expose volume to be mounted if needed
VOLUME c:/inetpub/wwwroot/logs