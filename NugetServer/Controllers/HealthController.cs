﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Http;

namespace NugetServer.Controllers
{
    public class HealthController : ApiController
    {
        [HttpGet]
        public string Check()
        {
            Trace.TraceInformation("HealthController.Check() entered");

            var envApiKey = Environment.ExpandEnvironmentVariables(
                     ConfigurationManager.AppSettings.Get(Global.apiKey));

            ConfigurationManager.AppSettings.Set(Global.apiKey, envApiKey);

            Trace.TraceInformation($"Transformed api key = {envApiKey}");

            return "Healthy";
        }
    }
}
