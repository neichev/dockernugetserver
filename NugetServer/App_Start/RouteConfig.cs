﻿using System.Web.Http;
using System.Web.Routing;

namespace NugetServer.App_Start
{
    public class RouteConfig
    {
        public static void Config(RouteCollection routes)
        {
            routes.MapHttpRoute(
                name: "health",
                routeTemplate: "health/",
                defaults: new { controller = "Health", action = "Check" });
        }
    }
}