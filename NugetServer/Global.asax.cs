﻿using NugetServer.App_Start;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Routing;

namespace NugetServer
{
    public class Global : System.Web.HttpApplication
    {
        internal static string apiKey = "apiKey";

        protected void Application_Start(object sender, EventArgs e)
        {
            NuGetODataConfig.Start(RouteTable.Routes);
            RouteConfig.Config(RouteTable.Routes);

            var envApiKey = Environment.ExpandEnvironmentVariables(
                     ConfigurationManager.AppSettings.Get(apiKey));

            ConfigurationManager.AppSettings.Set(apiKey, envApiKey);
        }
    }
}