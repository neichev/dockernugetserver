# Input
## Aim
To containerize NugetServer into docker and publish on Docker Hub within GitLab CI pipeline.

## Log
1. A solution made and pushed to GitLab repo.
2. Dockerfile was finished and tested locally.
3. Push to Docker Hub made from local.
4. gitlab-ci.yml added and filled with stages *docker-build*, *docker-push*
5. A shared windows runner was chosen with tags: **shared-windows**, **windows**, **windows-1809**
6. A pipeline stuck in the queue on execution because there are only 10 shared runners, only 2 for windows.
7. A decision made to go with on-prem GitLab runner. [Installation guide](https://docs.gitlab.com/runner/install/windows.html). Docker-windows executor was chosen.
8. Each job failed with `error code 2`. No specified error.
9. From the [doc](https://docs.gitlab.com/runner/executors/docker.html#supported-windows-versions) related to docker executor the only supported versions of Windows were specified. That’s *Windows Server 1809*, *Windows Server 1803*.
10. AWS EC2 instance with *Windows Server 2019 with Containers* was created under free tier (t2.micro).
11. Job failed with:
 ```
 docker : The term 'docker' is not recognized as the name of a cmdlet,
function, script file, or operable program. Check the spelling of the name, or 
if a path was included, verify that the path is correct and try again.
```
12. Service added to gitlab-ci.yml to be linked to the main container for runner with: 
```
services:
  - docker:stable-dind
```
13. The following error caught during pipeline run:
```
ERROR: Preparation failed: Error: No such image: docker:stable-dind (executor_docker.go:195:0s)
```
14. Tried locally and got an error regarding the absence of manifest for such image. A resolution was to turn on experimental features on Docker Engine: 
`"experimental": true`
15. Pulling docker image was succeeded but the following error caught:
```
ERROR: Preparation failed: Error response from daemon: failed to start service utility VM (createreadwrite): hcsshim::CreateComputeSystem ad57df9034e66daf37198c8cdc5544c841904b893be3ec6f87aa8753a00a129e_svm: The virtual machine could not be started because a required feature is not installed.
(extra info: {"SystemType":"container","Name":"ad57df9034e66daf37198c8cdc5544c841904b893be3ec6f87aa8753a00a129e_svm","Layers":null,"HvPartition":true,"HvRuntime":{"ImagePath":"C:\\Program Files\\Linux Containers","LinuxInitrdFile":"initrd.img","LinuxKernelFile":"kernel"},"ContainerType":"linux","TerminateOnLastHandleClosed":true}) (executor_docker.go:518:4s)
```
16. Looks like Linux kernel was tried to be deployed inside VM behind the scene but failed. Google said that one of the solution could be [this comment](https://github.com/docker/for-win/issues/574#issuecomment-341621969), to enable nested virtualization on EC2 instance. I did so and got the following error:
```
VM memory is set less than 4GB, without 4GB or more, you may not be able to start VMs.
```
17. Let’s try a bigger instance. I choosed t3a.large. The whole setup was made on this instance.
18. The same error came. So there are 
[several solutions](https://github.com/docker/for-win/issues/4470#issuecomment-519511295). 
There is [one](https://github.com/docker/for-win/issues/574#issuecomment-341621969) I applied already in 16th, 
that time I tried [Azure VM solution](https://stackoverflow.com/a/50099965) and it didn't work also.
19. By the way, I tried to switch to Linux containers and got failed with the following. It can relate.
```
Unable to start Hyper-V VM: 'DockerDesktopVM' failed to start.

Failed to start the virtual machine 'DockerDesktopVM' because one of the Hyper-V components is not running.

'DockerDesktopVM' failed to start. (Virtual machine ID CF827929-048A-4AFD-8C73-6E3D5BAB1A5D)

The Virtual Machine Management Service failed to start the virtual machine 'DockerDesktopVM' because one of the Hyper-V components is not running (Virtual machine ID CF827929-048A-4AFD-8C73-6E3D5BAB1A5D).
at Start-MobyLinuxVM, <No file>: line 688
at <ScriptBlock>, <No file>: line 811

   at Docker.Core.Pipe.NamedPipeClient.<TrySendAsync>d__5.MoveNext()
--- End of stack trace from previous location where exception was thrown ---
   at System.Runtime.ExceptionServices.ExceptionDispatchInfo.Throw()
   at Docker.Core.Pipe.NamedPipeClient.Send(String action, Object[] parameters)
   at Docker.Actions.<>c__DisplayClass39_0.<SwitchDaemon>b__0()
   at Docker.ApiServices.TaskQueuing.TaskQueue.<>c__DisplayClass18_0.<.ctor>b__1()
```

20. [Opened issue](https://github.com/docker/for-win/issues/4890) found.



